﻿
using BoardFrameworkImplementations;
using BoardGameFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BoardGameFrameworkUI
{
    public class BoardPiece : MonoBehaviour
    {
        public float speed;
        protected BoardPieceData data ;

        void Awake()
        {
            data =  new BoardPieceData();
            Debug.Log("I initiated");
            SetUpMovement();
        }

        void Update()
        {
            if (data.MovementControler != null) {
                data.MovementControler.MovementHandling(this);

            }
        }

        public virtual void SetUpMovement() {
            data.MovementControler = new BoardPieceMovementImplementation(speed,null);
        }

        public bool SetPositon(Vector3 newPosition, BoardSpace newCurrentSpace)
        {
            if (newCurrentSpace.AddOccupent(this)) {
                gameObject.transform.position = newPosition;
                data.currentSpace = newCurrentSpace;
                return true;

            }
            return false;
        }

        public void SetMovementControler(IBoardPieceMovement newControler) {//obsolete
            data.MovementControler = newControler;
        }

        public BoardSpace GetCurrentSpace()
        {
           return data.currentSpace;
        }

        public bool Move(List<BoardSpace> newPath)
        {
            if (data.MovementControler.IsPathEmpty()) {
                data.MovementControler.SetNewPath(newPath);
                return true;
            }
            else {
                return false;
            }
        }
    }

}