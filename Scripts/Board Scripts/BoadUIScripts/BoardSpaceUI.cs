﻿using BoardGameFrameworkCore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using BoardFrameworkInterfaces;

namespace BoardGameFrameworkUI
{
    public class BoardSpaceUI : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler,IPointerExitHandler
    {
        public BoardSpace spaceControler;

        #region delegates
        public delegate void ClickedEvent(BoardSpace spaceClicked);
        public ClickedEvent clickedEventHandler;

        public delegate void HoverBegunEvent(BoardSpace spaceHovered);
        public HoverBegunEvent hoverBeginEventHandler;


        public delegate void HoverEndEvent(BoardSpace spaceHovered);
        public HoverEndEvent hoverEndEventHandler;

        #endregion
        protected float clickdelay = 0.5f;
        protected float currentWaittime = 0f;

        protected IBoardFrameworkClickHandler clickControler;
        protected IBoardFrameworkHoverHandler hoverControler;
        // Start is called before the first frame update
        void Start()
        {
            

        }

        // Update is called once per frame
        void Update()
        {
            if (currentWaittime > 0) {
                currentWaittime -= Time.deltaTime;
            }
        }

        public virtual void SetupClickAndHoverControlers(IBoardFrameworkClickHandler clickerHandler, IBoardFrameworkHoverHandler hoverHandler) {
            clickControler = clickerHandler;
            hoverControler = hoverHandler;

        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (clickedEventHandler != null) {
                clickedEventHandler.Invoke(spaceControler);

            }

            if (currentWaittime <= 0) {
                currentWaittime = clickdelay;
                clickControler.PointerClickHandler(spaceControler);
            } else {
                currentWaittime = 0;
                clickControler.PointerDoubleClickHandler(spaceControler);
            }
            
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if(hoverBeginEventHandler!= null)
                hoverBeginEventHandler.Invoke(spaceControler);

            if (hoverControler != null)
            {
                hoverControler.PointerEnterHandler(this);
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (hoverEndEventHandler != null)
                hoverEndEventHandler.Invoke(spaceControler);
            
            if (hoverControler != null)
            {
                hoverControler.PointerExitHandler();
            }
        }
    }
}