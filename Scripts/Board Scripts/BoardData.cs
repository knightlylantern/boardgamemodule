﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFrameworkCore {

    [Serializable]
    public class BoardData
    {
        [SerializeField]
        List<RowsHelperFunction> boardSpaces;
        BoardType typeOfBoard;


      
        public BoardData(List<RowsHelperFunction> newSpaceList=null) {

            if (newSpaceList == null) {

                boardSpaces = new List<RowsHelperFunction>();
            }
            else {
                boardSpaces = newSpaceList;

            }
            typeOfBoard = BoardType.FOUR_WAY_MOVEMENT;
        }

        public BoardData(BoardType newBoardType, List<RowsHelperFunction> newSpaceList =null):this(newSpaceList)
        {
            typeOfBoard = newBoardType;
            if (newSpaceList == null) {
                return;
            }
            boardSpaces = newSpaceList;
                        
        }

             
        #region Getters

        public List<RowsHelperFunction> GetBoardSpaces() {
            return boardSpaces;
        }

        public BoardSpace GetSpecificSpace(int row, int column) {
            if (row > boardSpaces.Count || column > boardSpaces[0].GetColumns().Count) {
                return null;
            }
            return boardSpaces[row].GetColumns()[column];
        }


        public int GetRowAmount() {

            return boardSpaces.Count;

        }

        public int GetColumnAmount()
        {

            return boardSpaces[0].GetColumns().Count;

        }

        public BoardType GetBoardMovementType()
        {
            return typeOfBoard;
        }

        #endregion
    }
}