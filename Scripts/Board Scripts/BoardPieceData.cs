﻿using System.Collections;
using System.Collections.Generic;
using BoardGameFrameworkUI;
using UnityEngine;

namespace BoardGameFrameworkCore { 

    public class BoardPieceData
    {
        public BoardSpace currentSpace;
        public Vector3 currentGraphicalPosition;
        public IBoardPieceMovement MovementControler;
    }

}