﻿using BoardGameFrameworkUI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
namespace BoardGameFrameworkCore
{
    [Serializable]
    public class BoardSpace
    {
        [SerializeField]
        BoardSpaceData data;

        public BoardSpace(BoardSpaceData refData)
        {
            data = refData;

        }

        public BoardSpace(BoardSpaceData refData, int newRow, int newColumn)
        {
            data = new BoardSpaceData(newRow, newColumn);
            data.SetTraversableStatus(refData.GetIsTraversable());
            data.SetAmountOfOccupants(refData.GetPossibleOccupants());
            data.SetOccupants(refData.GetOccupants());
        }


        public virtual void SetNewNeighbours(List<BoardSpace> newNeighbours) {
            data.SetNeighbours(newNeighbours);

        }

        public virtual bool AddOccupent(BoardPiece boardPiece)
        {
            if (data.GetCanOccupy() && data.GetPossibleOccupants() > data.GetOccupants().Count) {
                data.AddOccupant(boardPiece);
            }
            else {
                return false;
            }

            if (data.GetPossibleOccupants() == data.GetOccupants().Count) {
                data.SetOccupyStatus(false);
            }
            return true;
        }

       
        public bool SetPassableStatus(bool newTraversableStatus) {
            data.SetTraversableStatus(newTraversableStatus);
            return data.GetIsTraversable();
        }

        public virtual void SetBoardUIReference(BoardSpaceUI boardSpaceUI)
        {
            data.SetBoardSpaceUI(boardSpaceUI);
        }

        public virtual void RemoveOccupant(int x) {
            data.RemoveOccupantAtIndex(x);
        }

        #region Getters
        public BoardSpaceUI GetBoardUIObject()
        {
            return data.GetSpaceUI();
        }


        public bool GetIsOccupiable()
        {
            return data.GetCanOccupy();
        }

        public bool GetIsTraversable()
        {
            return data.GetIsTraversable();
        }

        public int GetRowLocationInBoard()
        {
           return data.GetRow();
        }

        public int GetColumnLocationInBoard()
        {
            return data.GetColumn();
        }

        public List<BoardSpace> GetNeighbours()
        {
           return data.GetNeighbours();
        }
               
        public int GetSpaceCostToEnter()
        {
            return data.GetSpaceEntryCost();
        }

        public List<BoardPiece> GetOccupants() {
            return data.GetOccupants();
        }

        public T GetSpaceType<T>() where  T:BoardSpaceType
        {
            return (T)data.GetSpaceType();
        }
        #endregion

        public Sprite GetImage()
        {
            var sprite = data.GetSprite();

           return sprite;

        }
    }
}