﻿using BoardGameFrameworkUI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using UnityEngine;

namespace BoardGameFrameworkCore
{

    [Serializable]
    public class BoardSpaceData
    {
        public delegate void DataChangedEvent(BoardSpaceData data);
        public DataChangedEvent dataChangedEventHandler;

        [SerializeField]
        bool traversable = true;

        [SerializeField]
        bool canOccupy = true;

        [SerializeField]
        int amountOfPossibleOccupants = 1;

        [SerializeField][ReadOnly]
        int rowInBoard;

        [SerializeField][ReadOnly]
        int columnInBoard;

        [SerializeField]
        int spaceEntryCost=1;

        [SerializeField]
        List<BoardPiece> occupants = new List<BoardPiece>();

      //  [SerializeField]
        List<BoardSpace> neighbourSpaces;

        [SerializeField]
        BoardSpaceType typeOfSpace;

        #region UI elements
        [SerializeField]
        BoardSpaceUI graphicalSpaceReference;

        [SerializeField]
        Sprite boardspaceSprite;
        #endregion

        public BoardSpaceData(int row, int column, BoardSpaceUI newUISpace = null)
        {
            rowInBoard = row;
            columnInBoard = column;
            if (newUISpace != null)
            {
                graphicalSpaceReference = newUISpace;
            }
            /*else {
                graphicalSpaceReference = new BoardSpaceUI();
            }*/
        }

        #region setters

        public virtual bool SetAmountOfOccupants(int newAmount)
        {

            if (newAmount <= 0)
            {
                return false;
            }

            amountOfPossibleOccupants = newAmount;
            if (dataChangedEventHandler != null) {
                dataChangedEventHandler.Invoke(this);
            }
            return true;
        }

        public virtual bool SetBoardSpaceUI(BoardSpaceUI boardSpaceUI)
        {
            graphicalSpaceReference = boardSpaceUI;
            if (dataChangedEventHandler != null)
            {
                dataChangedEventHandler.Invoke(this);
            }
            return true;
        }

        public virtual bool SetOccupyStatus(bool newStatus) {
            canOccupy = newStatus;
            if (dataChangedEventHandler != null)
            {
                dataChangedEventHandler.Invoke(this);
            }
            return canOccupy;
        }

        public virtual bool SetOccupants(List<BoardPiece> newOccupants)
        {
            if (newOccupants == null)
            {
                return false;
            }
            occupants = newOccupants;
            if (dataChangedEventHandler != null)
            {
                dataChangedEventHandler.Invoke(this);
            }
            return true;
        }

        public virtual bool AddOccupant(BoardPiece newPiece)
        {
            occupants.Add(newPiece);
            if (dataChangedEventHandler != null)
            {
                dataChangedEventHandler.Invoke(this);
            }
            return true;
        }

        public virtual bool RemoveOccupantAtIndex(int index) {
            if (occupants.Count <= index) {
                return false;
            }
            occupants.RemoveAt(index);
            canOccupy = true;
            if (dataChangedEventHandler != null)
            {
                dataChangedEventHandler.Invoke(this);
            }
            return true;
        }

        public virtual bool SetTraversableStatus(bool newTraversableStatus)
        {
            if (newTraversableStatus == traversable)
            {
                return false;
            }
            traversable = newTraversableStatus;
            if (dataChangedEventHandler != null)
            {
                dataChangedEventHandler.Invoke(this);
            }
            return true;
        }

        public virtual bool SetNeighbours(List<BoardSpace>newNeighbours) {
            neighbourSpaces = newNeighbours;
            if (dataChangedEventHandler != null)
            {
                dataChangedEventHandler.Invoke(this);
            }
            return true;
        }

        public virtual bool SetSprite(Sprite newSprite) {
            boardspaceSprite = newSprite;
            if (dataChangedEventHandler != null)
            {
                dataChangedEventHandler.Invoke(this);
            }
            return true;
        }
        #endregion

        #region Getters
        public int GetRow()
        {
            return rowInBoard;
        }
        public int GetColumn()
        {
            return columnInBoard;
        }

        public int GetPossibleOccupants()
        {
            return amountOfPossibleOccupants;
        }

        public List<BoardPiece> GetOccupants()
        {
            return occupants;
        }

        public bool GetIsTraversable()
        {
            return traversable;
        }

        public List<BoardSpace> GetNeighbours() {
            return neighbourSpaces;
        }

        public bool GetCanOccupy() {
            return canOccupy;
        }

        public int GetSpaceEntryCost() {
            return spaceEntryCost;
        }

        public BoardSpaceUI GetSpaceUI()
        {
            return graphicalSpaceReference;
        }

        public BoardSpaceType GetSpaceType()
        {
            return typeOfSpace;
        }

        public Sprite GetSprite()
        {
            return boardspaceSprite;
        }

        #endregion

    }
}