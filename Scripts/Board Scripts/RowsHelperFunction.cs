﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFrameworkCore { 

[Serializable]
public class RowsHelperFunction 
{
        [SerializeField]
        List<BoardSpace> columns;

        public RowsHelperFunction() {
            columns = new List<BoardSpace>();
        }

        public RowsHelperFunction(List<BoardSpace> initialSpaces) {
            columns = new List<BoardSpace>(initialSpaces);

        }

        public List<BoardSpace> GetColumns() {

            return columns;
        }

        public void AddColumnSpace(BoardSpace newSpace) {
            columns.Add(newSpace);
        }
}
}