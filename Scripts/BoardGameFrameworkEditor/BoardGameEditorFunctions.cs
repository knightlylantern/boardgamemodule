﻿using BoardGameFrameworkCore;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace BoardGameFrameworkEditor
{
    [CustomEditor(typeof(LevelDesigner))]
    public class BoardGameEditorFunctions : Editor
    {

        public override void OnInspectorGUI()
        {
           
            base.OnInspectorGUI();

            LevelDesigner controler = (LevelDesigner)target;
            if (GUILayout.Button("Build Board") && controler.rows != 0 && controler.columns != 0)
            {
                controler.GenerateBoard();
            }
        }

        
    }
}

#endif