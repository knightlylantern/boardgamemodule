﻿using System.Collections;
using System.Collections.Generic;
using BoardGameFrameworkCore;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace BoardGameTests
{
    public class BoardTests
    {
        Board boardTest;

        [SetUp]
        public void Setup() {
           
            boardTest = new Board(2,3);
        }

        [Test]
        public void BoardGenerationTest()
        {
            Board boardTest = new Board(2,3);
            Assert.AreEqual(boardTest.GetBoardSize(), 6);

            Assert.AreEqual(boardTest.GetRowsSize(),2);

            Assert.AreEqual(boardTest.GetColumnsSize(), 3);


        }

        [Test]
        public void BoardGetSpacesTests()
        {
            
            Assert.AreEqual(boardTest.GetBoardSize(), 6);

            BoardSpace validSpace = boardTest.GetSpace(1, 2);
            Assert.IsNotNull(validSpace);

            BoardSpace invalidSpace = boardTest.GetSpace(2, 4);
            Assert.IsNull(invalidSpace);
        }

        [Test]
        public void BoardSpacesTraversableTests()
        {
            BoardSpace validSpace = boardTest.GetSpace(1, 2);
            Assert.IsTrue(validSpace.GetIsTraversable());

        }

        [Test]
        public void BoardSpacesLocationTests()
        {
            BoardSpace validSpace = boardTest.GetSpace(1, 2);
            Assert.AreEqual(validSpace.GetRowLocationInBoard(),1);
            Assert.AreEqual(validSpace.GetColumnLocationInBoard(), 2);
            
        }

        [Test]
        public void BoardSpaceFourNeighborsTest()
        {

            Assert.IsTrue(boardTest.SetUpBoardSpaceNeighbours());

            List<BoardSpace> boardSpaceNeighbors = boardTest.GetSpaceNeighbors(1, 2);

            Assert.AreEqual(boardSpaceNeighbors.Count, 2);

            boardSpaceNeighbors = boardTest.GetSpaceNeighbors(1, 1);
            Assert.AreEqual(boardSpaceNeighbors.Count, 3);
                        
        }

        [Test]
        public void BoardEightWaySpacesTest() {
            Board eightWaySpacesTestBoard = new Board(2, 3, BoardType.EIGHT_WAY_MOVEMENT);

            Assert.IsTrue(eightWaySpacesTestBoard.SetUpBoardSpaceNeighbours());

            List<BoardSpace> boardSpaceNeighbors = eightWaySpacesTestBoard.GetSpaceNeighbors(1, 2);

            Assert.AreEqual(3,boardSpaceNeighbors.Count);

            boardSpaceNeighbors = eightWaySpacesTestBoard.GetSpaceNeighbors(1, 1);
            Assert.AreEqual(5,boardSpaceNeighbors.Count);
        }
       

        
    }
}
