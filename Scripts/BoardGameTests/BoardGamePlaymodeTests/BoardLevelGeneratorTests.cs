﻿using System.Collections;
using System.Collections.Generic;
using BoardFrameworkImplementations;
using BoardGameFrameworkCore;
using BoardGameFrameworkEditor;
using BoardGameFrameworkUI;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace BoardGamePlaymodeTests
{
    public class BoardLevelGeneratorTests
    {
        LevelDesigner levelDesignControl;

        [SetUp]
        public void SetUpScene() {
            GameObject cam = new GameObject();
            cam.AddComponent<Camera>();

            GameObject designObject = new GameObject();
            levelDesignControl = designObject.AddComponent<LevelDesigner>();
            levelDesignControl.SpaceTemplate =  Resources.Load<SpriteRenderer>("BoardFrameworkTestResources/DummySprite");
            Debug.Log("Load");
        }
        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator BoardGeneratesUISpacesTest()
        {
            levelDesignControl.rows = 2;
            levelDesignControl.columns = 3;
            levelDesignControl.GenerateBoard();
            levelDesignControl.boardUISpaces[5].spaceControler.SetPassableStatus(false);
            yield return new WaitForSeconds(3);

            Assert.IsTrue(levelDesignControl.SetupGame());
            yield return new WaitForSeconds(1);

            Assert.AreEqual(levelDesignControl.GetBoard().GetSpace(1,2).GetIsTraversable(),levelDesignControl.boardUISpaces[5].spaceControler.GetIsTraversable());
            yield return null;
        }

        [UnityTest]
        public IEnumerator BoardPieceMovesCorrectlyTest()
        {
            levelDesignControl.rows =3;
            levelDesignControl.columns = 3;
            levelDesignControl.GenerateBoard();
            GameObject pieceContainer = new GameObject();

            BoardPiece testPiece = pieceContainer.AddComponent<BoardPiece>();
            Assert.IsTrue(levelDesignControl.SetupGame());

            yield return new WaitForSeconds(1);

            
            testPiece.SetPositon(levelDesignControl.boardUISpaces[0].transform.position, levelDesignControl.boardUISpaces[0].spaceControler);
            PathfindingControler pathFinder = new PathfindingControler();
            List<BoardSpace> path= pathFinder.GeneratePath(levelDesignControl.GetBoard(), testPiece.GetCurrentSpace(),levelDesignControl.GetBoard().GetSpace(2,2), 5);
            BoardPieceMovementImplementation mover = new BoardPieceMovementImplementation(1.5f,null);
            testPiece.SetMovementControler(mover);
            testPiece.Move(path);
            yield return new WaitForSeconds(3.5f);

            Assert.AreEqual(testPiece.GetCurrentSpace(), levelDesignControl.GetBoard().GetSpace(2, 2));
            Assert.AreEqual(testPiece.gameObject.transform.position, levelDesignControl.boardUISpaces[8].transform.position);
            yield return null;
        }
    }
}
