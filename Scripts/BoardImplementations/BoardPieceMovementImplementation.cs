﻿using BoardGameFrameworkCore;
using BoardGameFrameworkUI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardFrameworkImplementations
{

    public class BoardPieceMovementImplementation : IBoardPieceMovement
    {
        // Our pathfinding info.  Null if we have no destination ordered.
        List<BoardSpace> currentPath = new List<BoardSpace>();

        // How far this unit can move in one turn. Note that some tiles cost extra.
        float speed = 2;

        public BoardPieceMovementImplementation(float newSpeed, List<BoardSpace> initalPath) {
            speed = newSpeed;
            if (initalPath != null) {
                currentPath = initalPath;

            }
        }

        public bool IsPathEmpty()
        {
            if (currentPath.Count == 0)
            {
                return true;
            }
            else {
                return false;
            }
        }

        public bool MovementHandling(BoardPiece pieceToMove)
        {
            // Have we moved our visible piece close enough to the target tile that we can
            // advance to the next step in our pathfinding?
            // Move our position a step closer to the target.
            if (IsPathEmpty()) {
                return false; //you should not be calling me, as im empty, you should call ispathempty before calling me and should know this
            }

            float step = speed * Time.deltaTime; // calculate distance to move
            pieceToMove.transform.position = Vector3.MoveTowards(pieceToMove.transform.position, currentPath[0].GetBoardUIObject().transform.position, step);

            // Check if the position of the cube and sphere are approximately equal.
            if (Vector3.Distance(pieceToMove.transform.position, currentPath[0].GetBoardUIObject().transform.position) < 0.001f)
            {
                int indexOfPlayer = 0;
                bool foundPlayer = false;
                // hard code the position.
                for (int i = 0; i< pieceToMove.GetCurrentSpace().GetOccupants().Count; i++ ) {
                    if (pieceToMove.GetCurrentSpace().GetOccupants()[i] == pieceToMove) {
                         indexOfPlayer = i;
                        foundPlayer = true;
                        break;
                    }
                }
                if (foundPlayer) {
                    pieceToMove.GetCurrentSpace().RemoveOccupant(indexOfPlayer);

                }
                pieceToMove.SetPositon(currentPath[0].GetBoardUIObject().transform.position, currentPath[0].GetBoardUIObject().spaceControler);
                AdvancePathing();
            }
                        
            return true;
        }

        public void SetNewPath(List<BoardSpace> newPath)
        {
            currentPath =new List<BoardSpace>(newPath);
        }

        void AdvancePathing()
        {
            if (currentPath == null)
                return;
                        
            currentPath.RemoveAt(0);

          
        }
    }
}