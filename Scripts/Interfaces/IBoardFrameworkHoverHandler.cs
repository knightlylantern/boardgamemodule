﻿using BoardGameFrameworkCore;
using BoardGameFrameworkUI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardFrameworkInterfaces
{
    public interface IBoardFrameworkHoverHandler
    {
        void PointerEnterHandler(BoardSpaceUI spaceHovered);
        void PointerExitHandler();
    }
}