﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFrameworkCore
{
    public class PathfindingControler
    {
       
        public PathfindingControler()
        {

        }

             
        public virtual List<BoardSpace> GeneratePath(Board boardToUSe, BoardSpace startSpace, BoardSpace endSpace, int amountOfActions) {


            List<BoardSpace> pathToReturn = new List<BoardSpace>();
            List<RowsHelperFunction> boardSpaces = new List<RowsHelperFunction>(boardToUSe.GetFullBoardSpaces());


            Dictionary<BoardSpace, float> dist = new Dictionary<BoardSpace, float>();
            Dictionary<BoardSpace, BoardSpace> prev = new Dictionary<BoardSpace, BoardSpace>();

            // Setup the queue aka the list of nodes we haven't checked yet.
            List<BoardSpace> unvisited = new List<BoardSpace>();

            BoardSpace source = startSpace;

            BoardSpace target = endSpace;

            if (target.GetIsOccupiable() == false)
            {
                // We probably clicked on a mountain or something, so just quit out.
                return null;
            }

            dist[source] = 0;
            prev[source] = null;

            // Initialize everything to have INFINITY distance, since
            // we don't know any better right now. Also, it's possible
            // that some nodes CAN'T be reached from the source,
            // which would make INFINITY a reasonable value
            foreach (RowsHelperFunction row in boardSpaces)
            {
                List<BoardSpace> spacesOfRow = row.GetColumns();
                foreach (BoardSpace space in spacesOfRow) {
                    if (space != source)
                    {
                        dist[space] = Mathf.Infinity;
                        prev[space] = null;
                    }

                    unvisited.Add(space);
                }
               
            }

            while (unvisited.Count > 0)
            {
                // "u" is going to be the unvisited node with the smallest distance.
                BoardSpace nodeBeingTested = null;

                foreach (BoardSpace possibleU in unvisited) 
                {
                    if (nodeBeingTested == null || dist[possibleU] < dist[nodeBeingTested])//first time it always goes in
                    {
                        nodeBeingTested = possibleU;
                    }
                }

                if (nodeBeingTested == target)
                {
                    break;  // Exit the while loop!
                }

                unvisited.Remove(nodeBeingTested);

                foreach (BoardSpace v in nodeBeingTested.GetNeighbours())
                {
                    //float alt = dist[u] + u.DistanceTo(v);
                    float alt = dist[nodeBeingTested] + CostToEnterTile(nodeBeingTested, v);
                    if (alt < dist[v])
                    {
                        dist[v] = alt;
                        prev[v] = nodeBeingTested;
                    }
                }
            }

            // If we get there, the either we found the shortest route
            // to our target, or there is no route at ALL to our target.

            if (prev[target] == null)
            {
                // No route between our target and the source
                return null;
            }

           
            BoardSpace curr = target;

            // Step through the "prev" chain and add it to our path
            while (curr != null)
            {
                pathToReturn.Add(curr);
                curr = prev[curr];
            }

            // Right now, currentPath describes a route from out target to our source
            // So we need to invert it!

            if (pathToReturn.Count == 0) {//this check is to avoid the remove at 0 exception
                return pathToReturn;
            }
            pathToReturn.Reverse();
            pathToReturn.RemoveAt(0);//no need to have initial node in path
            
            return pathToReturn;

        }

        public virtual float CostToEnterTile(BoardSpace pointOfEntry, BoardSpace pointToEnter)
        {
            
            if ( pointToEnter.GetIsTraversable() == false)//pointToEnter.GetIsOccupiable() == false ||
                return Mathf.Infinity;

            float cost = pointToEnter.GetSpaceCostToEnter();
            
            if (pointOfEntry.GetRowLocationInBoard() != pointToEnter.GetRowLocationInBoard() && pointOfEntry.GetColumnLocationInBoard() != pointToEnter.GetColumnLocationInBoard())
            {
                // We are moving diagonally!  Fudge the cost for tie-breaking
                // Purely a cosmetic thing!
                cost += 0.001f;
            }

            return cost;

        }
    }
}